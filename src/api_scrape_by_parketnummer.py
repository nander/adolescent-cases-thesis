from data_engineering.api_scraping import SentenceScraperByParketnummer
import platform
import pandas as pd
from datetime import datetime

PARKETNUMMER_LIST = ['14%2F01680',
                     '16%2F01070']

sentence_by_case = SentenceScraperByParketnummer()


def get_keyword_list_information_in_csv_files(keyword_list):
    sentence_df = pd.DataFrame({"ECLI": [],
                                "Zaaknummer": []})
    for keyword in keyword_list:
        print(keyword)
        print(str(datetime.now()))
        df_temp = \
            sentence_by_case.from_parketnummer_to_text(
                keyword)
        print(df_temp)
        sentence_df = sentence_df.append(df_temp, ignore_index=True)

    sentence_df.to_csv(
        "output_{}_round2.csv".format("parketnummer"))


if platform.system() == 'Windows':
    if __name__ == "__main__":
        get_keyword_list_information_in_csv_files(
            keyword_list=PARKETNUMMER_LIST)
else:
    get_keyword_list_information_in_csv_files(
        keyword_list=PARKETNUMMER_LIST[:1])  # You can extend here
