# %%
import pandas as pd
from selenium import webdriver
import requests
import warnings
import time
from selenium.webdriver.chrome.options import Options

from data_engineering.shared import (
    call_API, get_detailed_link_from_ECLI, get_text_from_detailed_link,
    get_ecli_from_detailed_link, get_zaaknummer_from_called_api,
    DETAILED_LINK_URL
)

# Options of the Selenium scraper and Script
warnings.filterwarnings("ignore")
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_prefs = {}
chrome_options.experimental_options["prefs"] = chrome_prefs
chrome_prefs["profile.default_content_settings"] = {"images": 2}


# Variable assignments
BASE_URL = "https://uitspraken.rechtspraak.nl/#zoekverfijn/zt[0][zt]="\
    + "{}&zt[0][fi]=AlleVelden&zt[0][ft]=Alle+velden&dur[dr]=tussen"\
    + "&dur[da]=01-04-2015&dur[db]=01-09-2018&idx={}&so=Relevance&ps[]=ps1&"\
    + "rg[]=r3"
PARKETNUMMER_BASE_URL = "https://uitspraken.rechtspraak.nl/#zoekverfijn"\
    + "/zt[0][zt]={}&zt[0][fi]=Zaaknummer&zt[0][ft]=Zaak-%2F"\
    + "parketnummer&so=Relevance&ps[]=ps1"
ONE_LINK = "https://uitspraken.rechtspraak.nl/#zoekverfijn/dur[dr]=tussen&dur[da]="\
           "01-01-2015&dur[db]=01-09-2018&so=UitspraakDatumAsc&ps[]=ps1&cs[]=cs1&rg[]="\
           "r-4&ins[]=itTypeHr&ins[]=itRechtbank&idx=15000"


class SentenceScraperByKeyword:
    def get_result_count(self, keyword):
        driver = webdriver.Chrome(chrome_options=chrome_options)
        driver.set_window_size(1800, 1000)
        page_url = BASE_URL.format(keyword, str(3))
        print(page_url)
        driver.get(page_url)
        result_count = driver.find_element_by_class_name("resultcount").text
        driver.quit()
        print("Result count: {}".format(result_count))
        return result_count

    def get_all_keyword_related_detailed_links(self, keyword):
        result_count = self.get_result_count(keyword=keyword)
        driver = webdriver.Chrome(chrome_options=chrome_options)
        driver.set_window_size(1800, 1000)
        result_page_url = BASE_URL.format(keyword, str(result_count))
        driver.get(result_page_url)
        time.sleep(50)
        links = [link_element.get_attribute("href").split("&")[0]
                 for link_element in driver.find_elements_by_class_name(
                     "titel")]
        driver.quit()
        return links

    def get_text_zaaknummer_ecli_given_keyword(self, keyword):
        detailed_link_list = self.get_all_keyword_related_detailed_links(
            keyword)
        text_df = pd.DataFrame({"ECLI": [],
                                "Zaaknummer": [],
                                "Text": []})
        for detailed_link in detailed_link_list:
            ecli_temp = get_ecli_from_detailed_link(detailed_link)
            detailed_sentence = requests.get(
                DETAILED_LINK_URL.format(ecli_temp))
            try:
                zaaknummer_temp = get_zaaknummer_from_called_api(
                    detailed_sentence)
            except Exception as e:
                print(e)
                zaaknummer_temp = ""
                pass
            text_temp = get_text_from_detailed_link(detailed_sentence)
            df_temp = pd.DataFrame({"ECLI": [ecli_temp],
                                    "Zaaknummer": [zaaknummer_temp],
                                    "Text": [text_temp]})
            text_df = text_df.append(df_temp, ignore_index=True)
        return text_df


class SentenceScraperByParketnummer:
    def get_links_from_parketnummer(self, parketnummer):
        parketnummer_query_link = PARKETNUMMER_BASE_URL.format(
            parketnummer)
        print(parketnummer_query_link)
        driver = webdriver.Chrome(chrome_options=chrome_options)
        driver.set_window_size(1800, 1000)
        driver.get(parketnummer_query_link)
        time.sleep(20)
        links = [link_element.get_attribute("href").split("&")[0]
                 for link_element in driver.find_elements_by_class_name(
                     "titel")]
        driver.quit()
        return links

    def get_detailed_links_from_parketnummer(self, parketnummer):
        links = self.get_links_from_parketnummer(parketnummer)
        detailed_links = []
        for link in links:
            temp_ECLI = get_ecli_from_detailed_link(link)
            detailed_links.append(temp_ECLI)
        return detailed_links

    def from_parketnummer_to_text(self, parketnummer):
        detailed_links = self.get_detailed_links_from_parketnummer(
            parketnummer)
        print(detailed_links)
        text_df = pd.DataFrame({"ECLI": [],
                                "Zaaknummer": []})
        for ECLI in detailed_links:
            df_temp = pd.DataFrame({"ECLI": [ECLI],
                                    "Zaaknummer": [parketnummer]})
            text_df = text_df.append(df_temp, ignore_index=True)
        return text_df


class SentenceScraperByOneLink:
    def get_all_keyword_related_detailed_links(self):
        driver = webdriver.Chrome(chrome_options=chrome_options)
        driver.set_window_size(1800, 1000)
        driver.get(ONE_LINK)
        print(ONE_LINK)
        time.sleep(20)
        links = [link_element.get_attribute("href").split("&")[0]
                 for link_element in driver.find_elements_by_class_name(
                     "titel")]
        driver.quit()
        return links
