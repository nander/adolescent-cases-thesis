
import pandas as pd
import platform
import time

from api_scraping import SentenceScraperByParketnummer


PARKETNUMMER_CSV_PATH = "./data/{}"
PARKETNUMMER_FILE_NAME = "parketnummers_from_pdf_names.csv"

scraper = SentenceScraperByParketnummer()


def retrieve_text_information_for_parketnummers(
        parketnummer_file_name):
    start = time.time()
    p_nummer_df = pd.read_csv(PARKETNUMMER_CSV_PATH.format(
        parketnummer_file_name))
    zaaknummer_list = list(set(p_nummer_df.zaaknummers.values))
    print("Retrieval started.")
    print("\nZaaknummer list contains {} unique instances in total.\n".format(
        len(zaaknummer_list)))
    text_information_df = pd.DataFrame({"ECLI": [],
                                        "Zaaknummer": [],
                                        "Text": []})
    for zaaknummer in zaaknummer_list:
        scraped_df_temp = scraper.from_parketnummer_to_text(zaaknummer)
        text_information_df = text_information_df.append(
            scraped_df_temp, ignore_index=True)
    print("File is being written...\n")
    text_information_df.to_csv(
        PARKETNUMMER_CSV_PATH.format("output.csv", sep="\t", index=False))
    end = time.time()
    time_spent = (end - start) / 60
    print("Script took {} minutes to execute.".format(time_spent))
    return text_information_df


if platform.system() == 'Windows':
    if __name__ == "__main__":
        retrieve_text_information_for_parketnummers(PARKETNUMMER_FILE_NAME)
else:
    retrieve_text_information_for_parketnummers(PARKETNUMMER_FILE_NAME)
