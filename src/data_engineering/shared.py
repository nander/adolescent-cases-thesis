
import requests
from lxml import html
import xmltodict


DETAILED_LINK_URL = "https://data.rechtspraak.nl/uitspraken/content?id={}"
XPATH_QUERY = "*//para//node()"
ELEMENT_UNICODE = "<class 'lxml.etree._ElementUnicodeResult'>"


def call_API(URL):
    try:
        page = requests.get(URL)
        return page
    except Exception as e:
        print(e)


def get_detailed_link_from_ECLI(ECLI):
    return DETAILED_LINK_URL.format(ECLI)


def get_text_from_detailed_link(called_api_page):
    try:
        detailed_page_element_tree = html.fromstring(
            called_api_page.content)
        reviews = detailed_page_element_tree.xpath(
            XPATH_QUERY)
    except Exception as e:
        print("{error} happend".format(error=e))
    return " ".join(
        [rev for rev in reviews
            if str(type(rev)) == ELEMENT_UNICODE])


def get_ecli_from_detailed_link(detailed_link):
    try:
        return detailed_link.split("=")[-1]
    except Exception as e:
        return None


def get_zaaknummer_from_called_api(called_api_page):
    try:
        doc = xmltodict.parse(called_api_page.text)
        description = doc["open-rechtspraak"]["rdf:RDF"]["rdf:Description"][0]
        return description["psi:zaaknummer"]["#text"]
    except Exception as e:
        print(e + " | occured | " + called_api_page)
        return ""
