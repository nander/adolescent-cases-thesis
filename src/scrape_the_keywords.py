from data_engineering.api_scraping import SentenceScraperByKeyword
import platform

KEYWORD_LIST = ["ASR"]

sentence_by_keyword = SentenceScraperByKeyword()


def url_fix(text_with_spaces):
    separated_strings = text_with_spaces.split(" ")
    return "+".join(separated_strings)


def get_keyword_list_information_in_csv_files(keyword_list):
    fixed_keyword_list = [url_fix(keyword) for keyword in keyword_list]
    for keyword in fixed_keyword_list:
        sentence_df = \
            sentence_by_keyword.get_text_zaaknummer_ecli_given_keyword(
                keyword)
        print("Retrieval has been finished for {}".format(keyword))
        print("Shape: {}".format(str(sentence_df.shape)))
        sentence_df.iloc[:5000, :].to_csv(
            "output_{}_round2.csv".format(keyword))


if platform.system() == 'Windows':
    if __name__ == "__main__":
        get_keyword_list_information_in_csv_files(keyword_list=KEYWORD_LIST)
else:
    get_keyword_list_information_in_csv_files(keyword_list=KEYWORD_LIST)
