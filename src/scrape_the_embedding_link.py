from data_engineering.api_scraping import SentenceScraperByOneLink
import platform

sentence_by_link = SentenceScraperByOneLink()


def get_list_information_in_csv_files():
    import pandas as pd
    sentence_df = \
        sentence_by_link.get_all_keyword_related_detailed_links()
    print("Retrieval has been finished for {}".format("link"))
    print("Shape: {}".format(str(len(sentence_df))))
    pd.DataFrame(sentence_df).to_csv(
        "output_{}_round_2_2nd_iter.csv".format("link"), sep=",", index=False)


if platform.system() == 'Windows':
    if __name__ == "__main__":
        get_list_information_in_csv_files()
else:
    get_list_information_in_csv_files()
