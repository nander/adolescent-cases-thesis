from sklearn.base import BaseEstimator, TransformerMixin
from nltk.tokenize.regexp import RegexpTokenizer

from pathlib import Path
import os
from copy import deepcopy
import re

import warnings
warnings.filterwarnings("ignore")


PARENT_PATH = os.fspath(Path(__file__).parents[1])
STOPWORDS_FILE_PATH = os.path.join(PARENT_PATH,
                                   "stopwords",
                                   "stopwords.txt")

REGEX_FOR_TEXT = '''(?x)             # set flag to allow verbose regexps
                    (?:[A-Z]\.)+      # abbreviations, e.g. U.S.A.
                    |\w+(?:-\w+)*        # words with optional internal hyphens
                    |\.\.\.              # ellipsis
                    '''


def get_money_and_date_regex_for_dutch():

    month_labels_in_dutch = [
        "januari", "februari", "maart", "april", "mei", "juni", "juli",
        "augustus", "september", "oktober", "november", "december"
    ]

    def REGEX_TEXT_FOR_DATE(x): return r"([0-9]{2}[\ ](" + x + ")[\ ][0-9]{4})"

    date_regex_for_dutch = [REGEX_TEXT_FOR_DATE(
        ml) for ml in month_labels_in_dutch]

    money_regex = r"([\$|\€][\ ][\d\.\,]+)"

    date_regex = "|".join(date_regex_for_dutch)

    return money_regex, date_regex


def transform_the_text(text):
    MONEY_REGEX, DATE_REGEX = get_money_and_date_regex_for_dutch()
    text_copy = deepcopy(text)

    text_copy = text_copy.\
        replace("#text", "").\
        replace("('@role', 'bold')", "").\
        replace("('@role', 'italic')", "").\
        replace("emphasis", "").\
        replace("OrderedDict", "")

    text_copy = re.sub(string=text_copy, repl="__DATE__", pattern=DATE_REGEX)
    text_copy = re.sub(string=text_copy, repl="__MONEY__", pattern=MONEY_REGEX)

    return text_copy


def custom_tokenizer(text):
    clean_text = re.sub("(sec [0-9])", "", text)
    rp = RegexpTokenizer(REGEX_FOR_TEXT)
    clean_text = transform_the_text(clean_text)
    tokenized_words = rp.tokenize(clean_text)
    return [word.lower()
            for word in tokenized_words
            if (word.isalpha() or word == "__DATE__" or word == "__MONEY__")]


def get_stopwords():
    with open(STOPWORDS_FILE_PATH) as f:
        stopwords = f.read().split('\n')
    return stopwords


class ColumnExtractor(BaseEstimator, TransformerMixin):
    """Takes a dataframe, extracts a columns as feature """

    def __init__(self, column_names):
        self.column_names = column_names

    def transform(self, df, y=None):
        """Return the columns"""
        matching_columns = [
            t for t in df.columns
            if t.startswith(self.column_names)]
        return df.loc[:, matching_columns].values

    def fit(self, df, y=None):
        """Pass"""
        return self


class TextExtractor(BaseEstimator, TransformerMixin):
    """Takes a dataframe, extracts a columns as feature """

    def transform(self, df, y=None):
        """Return the columns"""
        return df.loc[:, "Text"].values.ravel()

    def fit(self, df, y=None):
        """Pass"""
        return self


class ToDenseTransformer(BaseEstimator, TransformerMixin):
    """To center the data sklearn StandardScaler needs dense arrays.

    As a result and solution to this problem the sparse arrays are
    transformed by using to"""

    def transform(self, sparse_matrix, y=None):
        """Return the dense matrix"""
        return sparse_matrix.toarray()

    def fit(self, sparse_matrix, y=None):
        """Pass"""
        return self
