
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD, LatentDirichletAllocation
from sklearn.preprocessing import StandardScaler
import numpy as np
import json
from pathlib import Path
import os

from pipeline.feature_construction import (
    custom_tokenizer, get_stopwords, ColumnExtractor, ToDenseTransformer,
    TextExtractor)

TF_IDF_NGRAM_RANGE = (1, 2)
USE_IDF = True
RANDOM_SEED = np.random.seed(42)
STOPWORDS = get_stopwords()
MAX_FEATURES = 4000

PARENT_PATH = os.fspath(Path(__file__).parents[1])
PATH_FOR_STRUCTURED_DB_LABELS = os.path.join(
    PARENT_PATH,
    "pipeline",
    "pipeline_data",
    "column_type_matches_20190202.json")


class MakePipeline:

    def set_sentence_classifier_pipeline(self,
                                         text_option,
                                         skip_dimension_reduction,
                                         reduction_type,
                                         n_components,
                                         selections,
                                         classifier,
                                         topic_combination=None):
        """Initializes the sentence classification pipeline
        by using the structured and unstructured data combined
        with the help of FeatureUnion.

        Inherits the same methods that the classifier contains
        while predicting.
        """
        if (skip_dimension_reduction is True) and (text_option):
            # If text is being used without dimension reduction:
            # Then we have a sparse matrix.
            # Before the centering, we need to transform the sparse
            # matrix into a dense array.
            pipeline = Pipeline([
                ('features', self.set_feature_pipeline(
                    text_option=text_option,
                    skip_dimension_reduction=skip_dimension_reduction,
                    reduction_type=reduction_type,
                    n_components=n_components,
                    selections=selections,
                    topic_combination=topic_combination)),
                ('dense_matrix', ToDenseTransformer()),
                ('scaler', StandardScaler(with_mean=True, with_std=True)),
                ('classification', classifier)
            ])

        else:
            # Otherwise we skip the dense array option.
            pipeline = Pipeline([
                ('features', self.set_feature_pipeline(
                    text_option=text_option,
                    skip_dimension_reduction=skip_dimension_reduction,
                    reduction_type=reduction_type,
                    n_components=n_components,
                    selections=selections,
                    topic_combination=topic_combination)),
                ('scaler', StandardScaler(with_mean=True, with_std=True)),
                ('classification', classifier)
            ])
        return pipeline

    def set_feature_pipeline(self,
                             text_option,
                             skip_dimension_reduction,
                             reduction_type,
                             n_components,
                             selections,
                             topic_combination=None):
        """Combines the structured and unstructured data.
        """
        db_pipe_class = DBFeaturePipeline(selections=selections)

        text_pipe_class = TextFeaturePipeline(
            skip_dimension_reduction=skip_dimension_reduction,
            reduction_type=reduction_type,
            n_components=n_components)

        topic_class = TextFeaturePipeline(
            skip_dimension_reduction=False,
            reduction_type="lda",
            n_components=n_components)

        feature_union_list = [('text', text_pipe_class.text_transformer)]

        if text_option is None and topic_combination is None:
            return FeatureUnion(db_pipe_class.set_pipeline_for_db())

        elif (text_option is not None) and (selections is not None) and (
                topic_combination is None):
            feature_union_list.extend(db_pipe_class.set_pipeline_for_db())
            return FeatureUnion(feature_union_list)

        elif (text_option is not None) and (selections is None) and (
                topic_combination is None):
            return FeatureUnion(feature_union_list)

        elif (text_option is not None) and (selections is None) and (
                topic_combination is not None):
            feature_union_list.extend(
                [('text_topic', topic_class.text_transformer)])
            return FeatureUnion(feature_union_list)


class TextFeaturePipeline:
    def __init__(self,
                 skip_dimension_reduction,
                 reduction_type, n_components):
        self.skip_dimension_reduction = skip_dimension_reduction
        self.reduction_type = reduction_type
        self.n_components = n_components
        self.text_transformer = self.set_features_for_text(
            skip_dimension_reduction=self.skip_dimension_reduction,
            reduction_type=self.reduction_type,
            n_components=self.n_components
        )

    def set_features_for_text(self,
                              skip_dimension_reduction,
                              reduction_type,
                              n_components):
        """Combines the text pre-processing steps into one pipeline.

        The dataframe must contain a column named 'Text'.
        """
        # It does not return a tf-idf vector,
        # if the reduction type is "word2vec"
        text_pipeline = Pipeline([
            ('extract_text', TextExtractor()),
            ('tf_idf', self.set_tf_idf()),
            ('dimension_reduction', self.set_dimension_reduction(
                skip_dimension_reduction=skip_dimension_reduction,
                reduction_type=reduction_type,
                n_components=n_components)
             )
        ])
        return text_pipeline

    def set_tf_idf(self):
        """Sets up the Term Frequency and Inverse Document Frequency
        Vectorizor with the given settings."""
        tfidf = TfidfVectorizer(lowercase=True,
                                max_features=MAX_FEATURES,
                                stop_words=STOPWORDS,
                                use_idf=USE_IDF,
                                ngram_range=TF_IDF_NGRAM_RANGE,
                                tokenizer=custom_tokenizer)
        return tfidf

    def set_dimension_reduction(self,
                                skip_dimension_reduction=False,
                                reduction_type=None,
                                n_components=None):
        """Sets up the dimension reduction technique that is
        intended to be used in the text pipeline.

        If skip_dimension_reduction is set to be True, returns
        to None. And do not apply dimension reduction in the
        text pipeline.
        """
        if skip_dimension_reduction is False:
            if self.reduction_type == "lsa":
                reduce_dimensions = TruncatedSVD(
                    n_components=n_components)
                return reduce_dimensions
            elif self.reduction_type == "lda":
                reduce_dimensions = LatentDirichletAllocation(
                    n_components=n_components)
                return reduce_dimensions
            else:
                return None
        return None


class DBFeaturePipeline:

    def __init__(self, selections):
        self.selections = selections
        self.pipe = self.set_pipeline_for_db

    def set_pipeline_for_db(self):
        '''Gets the selection parameter from the Class Instantiation.

        Sets the FeatureUnion in such a way that the selected Type of columns
        are inserted into the Structured DB part of the pipeline.'''
        if self.selections is not None:
            with open(PATH_FOR_STRUCTURED_DB_LABELS, "r") as pr:
                df = json.loads(pr.read())

            df.update({"WIE": "WIE", "WE": "WE", "WI": "WI"})
            list_of_columns_to_extract = [
                k for k, v in df.items() if v in self.selections]
            # Here we need to filter the columns to be added
            return [self.tuple_formula(k)
                    for k in list_of_columns_to_extract]
        return None

    def tuple_formula(self, text):
        return (text, ColumnExtractor(text))
