
import pandas as pd
from copy import deepcopy
import datetime
import re

TEXT_COLUMNS = ["content_details", "indictment", "full_text"
                "evidence", "punishment", "decision", "Text"]


def voeg_indicator(row):
    try:
        if row == "gevoegd":
            return 1
        else:
            return 0
    except Exception as e:
        return 0


def count_SR(row):
    try:
        return row.count("SR")
    except Exception as e:
        return 0


def get_date_columns(dataframe):
    return [x for x in list(dataframe.columns) if "DAT" in x]


def get_ja_nee_columns(dataframe):
    '''Yes or no columns are extracted based on:

    If they include at least one "nee" in the dataframe.'''
    ja_nee_columns = []

    for col_name, the_type in zip(dataframe.columns, dataframe.dtypes):
        if the_type == "object":
            if set(
                [str(x) for x in dataframe[col_name].values]
            ).issubset({"ja", "nee", "nan", "0.0", "1.0"}):
                ja_nee_columns.append(col_name)
    return ja_nee_columns


def missing_value_handling(dataframe):
    '''Only handles ja of nee columns, integers and floats.

    Since there is a problem for LFTINS in the dataset,
    before starting the function we also fix it.

    Null values in the integers and floats columns are filled with 0.

    Null values in the ja of nee columns are filled with "nee".'''
    def if_else_lambda(row):
        if str(row) == "nee":
            return 0.0
        elif str(row) == "ja":
            return 1.0
        elif str(row) == "nan":
            return 0.0
        return float(row)

    dt_fr = dataframe.copy(deep=True)
    dt_fr["LFTINS"] = dt_fr.LFTINS.apply(lambda x: float(
        "".join(x.split("."))[:2]+"."+"".join(x.split("."))[2:]))
    dt_fr["LFTINSZ1"] = dt_fr.LFTINSZ1.apply(lambda x: float(
        "".join(x.split("."))[:2]+"."+"".join(x.split("."))[2:]))
    dt_fr["LFTINSZ1INCLVTT"] = dt_fr.LFTINSZ1INCLVTT.apply(lambda x: float(
        "".join(x.split("."))[:2]+"."+"".join(x.split("."))[2:]))
    ja_nee_columns = get_ja_nee_columns(dt_fr)
    date_columns = get_date_columns(dt_fr)
    for col_name, the_type in zip(dt_fr.columns, dt_fr.dtypes):
        if col_name in ja_nee_columns:
            dt_fr[col_name] = dt_fr[col_name].apply(if_else_lambda)
        if (str(the_type) in ["int64", "float64"]) and (
                col_name not in date_columns + ["LABEL"]):
            if dt_fr[col_name].isnull().sum() > 0:
                dt_fr.loc[dt_fr[col_name].isnull(), col_name] = 0
    return dt_fr


def categorical_integer_handling(dataframe):
    '''Some columns have the datatype of integer,

    But those indicate a categorical information.

    This function transforms those integer values into
    object values for binarization.
    '''
    dt_fr = dataframe.copy(deep=True)

    categorical_integer_variables = []

    for col_name in categorical_integer_variables:
        dt_fr[col_name] = dt_fr[col_name].astype(object)

    return dt_fr


def create_art_comb_col(dataframe):
    '''Some columns indicate the related Articles in the
    Dutch Criminal Law.

    Those articles are aggregated by counting how many articles in total.

    Then the Article related columns are dropped.
    '''
    dt_fr = dataframe.copy(deep=True)
    article_columns = ["ARTCOMB1", "ARTCOMB2", "ARTCOMB3",
                       "ARTCOMB4", "ARTCOMB5", "ARTCOMB6",
                       "ARTCOMB7"]

    def drop_article_columns(dataframe):
        return dataframe.drop(article_columns, axis=1)
    dt_fr["article_count"] = 0
    for col_name in article_columns:
        temp_count_series = dt_fr[col_name].apply(count_SR)
        dt_fr["article_count"] += temp_count_series
    return drop_article_columns(dt_fr)


def create_date_features(dataframe):
    '''Creates:

    DIFF_BIRTH_BEG: Difference between the Beginning of the Pre-Trial
    Detention and The Decision

    If the value is missing, then the datetime is fixed to the

    1st April 2014 which is date the 77c is changed.

    Deletes:

    ALL OTHER DATE COLUMNS
    '''
    def convert_integer_to_datetime(row):
        try:
            return datetime.datetime.utcfromtimestamp(row/1000000000)
        except ValueError as v:
            return datetime.datetime(2014, 4, 1, 0, 0, 0)
    dat_cols = set(get_date_columns(dataframe)) - {"OVDAT"}
    dataframe["DIFF_BIRTH_BEG"] = dataframe.apply(
        lambda x: (convert_integer_to_datetime(
            x["GEBDAT"]) - convert_integer_to_datetime(
                x["DATBEGVA"])).days, axis=1)
    return dataframe.drop(dat_cols, axis=1)


def aggregator_of_SCM_cols(dataframe):

    dt_fr = dataframe.copy(deep=True)

    def aggregator_lambda(x): return list(
        {str(element).replace(" ", "_").lower().replace(
            ".", "").replace("(", "").replace(")", "").replace(
                ",", "").replace("/", "").replace("-", "_")
         for element in
         [x["SCM2010_1"], x["SCM2010_2"],
          x["SCM2010_3"], x["SCM2010_4"],
          x["SCM2010_5"], x["SCM2010_6"],
          x["SCM2010_7"]]
         if str(element) != "nan"}
    )

    dt_fr["AGG_SCM"] = dt_fr.apply(aggregator_lambda, axis=1)

    list_of_SCM = list(
        {str(v)
         for c in dt_fr["AGG_SCM"].values
         for v in c}
    )

    for new_col in list_of_SCM:
        dt_fr[new_col] = 0

    for idx, l in enumerate(dt_fr["AGG_SCM"].values):
        if len(l) > 0:
            for crime in l:
                dt_fr.loc[idx, crime] = 1

    cols_to_be_dropped = ["SCM2010_1", "SCM2010_2",
                          "SCM2010_3", "SCM2010_4",
                          "SCM2010_5", "SCM2010_6",
                          "SCM2010_7", "AGG_SCM"]

    dt_fr = dt_fr.drop(cols_to_be_dropped, axis=1)

    return dt_fr


def aggregator_of_DGRCLAS_cols(dataframe):

    dt_fr = dataframe.copy(deep=True)

    def aggregator_lambda(x): return list(
        {str(element).replace(" ", "_").lower().replace(
            ".", "").replace("(", "").replace(")", "").replace(
                ",", "").replace("/", "").replace("-", "_")
         for element in
         [x["DGRCLAS21"], x["DGRCLAS22"],
          x["DGRCLAS23"], x["DGRCLAS24"],
          x["DGRCLAS25"], x["DGRCLAS26"],
          x["DGRCLAS27"]]
         if str(element) != "nan"}
    )

    dt_fr["AGG_DGR"] = dt_fr.apply(aggregator_lambda, axis=1)

    list_of_SCM = list(
        {str(v).replace(" ", "_").lower().replace(
            ".", "").replace("(", "").replace(")", "").replace(
                ",", "").replace("/", "")
         for c in dt_fr["AGG_DGR"].values
         for v in c}
    )

    for new_col in list_of_SCM:
        dt_fr[new_col] = 0

    for idx, l in enumerate(dt_fr["AGG_DGR"].values):
        if len(l) > 0:
            for crime in l:
                dt_fr.loc[idx, crime] = 1

    cols_to_be_dropped = ["DGRCLAS21", "DGRCLAS22",
                          "DGRCLAS23", "DGRCLAS24",
                          "DGRCLAS25", "DGRCLAS26",
                          "DGRCLAS27", "AGG_DGR"]

    dt_fr = dt_fr.drop(cols_to_be_dropped, axis=1)

    return dt_fr
