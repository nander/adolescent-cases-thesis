import pandas as pd
from pathlib import Path
import os

from data_preprocessing.pre_process_functions import (
    voeg_indicator,
    missing_value_handling,
    categorical_integer_handling,
    create_art_comb_col,
    create_date_features,
    aggregator_of_SCM_cols,
    aggregator_of_DGRCLAS_cols)

import warnings
warnings.filterwarnings("ignore")

PARENT_PATH = os.fspath(Path(__file__).parents[2])
PATH = os.path.join(PARENT_PATH,
                    "data",
                    "model_experiment",
                    "dataset_full_20190204.csv")

PATH_FOR_WORD2VEC = os.path.join(PARENT_PATH,
                                 "data",
                                 "model_experiment",
                                 "word2vec_20190318.csv")

# The four labels:
# 1. Personal
# 2. Criminal
# 3. Procedural
# 4. Punishments
PATH_FOR_STRUCTURED_DB_LABELS = os.path.join(
    PARENT_PATH,
    "data",
    "model_experiment",
    "dataset_labels_20190201.xlsx")

DROPABLE_COLS = ["LABEL", "LFTBEGREC", "COHORT", "VOEGING_FOUT",
                 "SZAAK_SPSS", "PARKETNR_ENC_ID_HOF", "INDRK",
                 "SZAAK_EA_BIJ_HB"]

POSSIBLE_SECTIONS = ["content_details", "indictment",
                     "evidence", "punishment", "decision", "full_text"]


class DataPrep:
    def __init__(self, tag, keep_text, sections):

        self.data = self.transformDB(
            dataframe=self.get_dataset_and_labels()[0],
            tag=tag, keep_text=keep_text, sections=sections)
        self.labels = self.get_dataset_and_labels()[1]

    # This part gets the dataset and labels.
    def get_dataset_and_labels(self):
        df = pd.read_csv(PATH, sep="|")

        return df, df.LABEL.values

    def preprocess_structured_data(self, dataframe):
        dt_fr = dataframe.copy(deep=True)

        def geb_land(row):
            top_4_frequent = list(dt_fr.GEBLAND.value_counts()[:4].index)
            try:
                if row in top_4_frequent:
                    return str(row)
                else:
                    return "Other"
            except Exception as e:
                return "Other"

        dt_fr["GEBLAND"] = dt_fr.GEBLAND.apply(geb_land)

        dt_fr["SRTVOEG"] = dt_fr.SRTVOEG.apply(voeg_indicator)

        dt_fr = missing_value_handling(dt_fr)
        dt_fr = categorical_integer_handling(dt_fr)
        dt_fr = create_art_comb_col(dt_fr)
        dt_fr = create_date_features(dt_fr)
        dt_fr = aggregator_of_SCM_cols(dt_fr)
        dt_fr = aggregator_of_DGRCLAS_cols(dt_fr)
        return dt_fr

    def binarize_without_text_then_merge_text(self, dataframe, sections):
        dataframe_without_text = dataframe.drop(DROPABLE_COLS, axis=1)
        dataframe_without_text_bin = pd.get_dummies(
            dataframe_without_text, drop_first=True)
        if len(sections) == 1:
            dataframe_without_text_bin["Text"] = self.text_dataset.apply(
                lambda x: x[sections[0]], axis=1)

        elif len(sections) > 1:
            dataframe_without_text_bin["Text"] = self.text_dataset.apply(
                lambda x:
                " ".join([str(a) for a in x[sections]]), axis=1)
        return dataframe_without_text_bin

    def binarize_without_text(self, dataframe):
        dataframe_without_text = dataframe.drop(DROPABLE_COLS, axis=1)
        return pd.get_dummies(
            dataframe_without_text, drop_first=True)

    def transformDB(self, dataframe, tag=None, keep_text=None, sections=None):
        assert (keep_text in [True, False, None]
                ), "Give a valid keep_text option."

        # First the structured dataset
        dataframe = self.preprocess_structured_data(dataframe)

        # Keep the text separate
        self.text_dataset = dataframe.loc[:, POSSIBLE_SECTIONS]

        # Then handles the textual information
        if tag is not None:
            structured_db_labels = pd.read_excel(PATH_FOR_STRUCTURED_DB_LABELS)
            tagged_columns = structured_db_labels.loc[
                structured_db_labels.label.isin(tag),
                "column_name"].values.tolist()
            dataframe = dataframe.loc[:, list(
                set(tagged_columns+DROPABLE_COLS+["index"]))]

        if keep_text is True:
            if (sections is not None) and (
                    sections not in [["WE"], ["WI"], ["WIE"]]):
                return self.binarize_without_text_then_merge_text(
                    dataframe=dataframe, sections=sections)

            # Text could be Word2Vec as well
            if sections in [["WE"], ["WI"], ["WIE"]]:
                dataframe_for_word2vec = pd.read_csv(
                    PATH_FOR_WORD2VEC, sep="|")
                dataframe = self.binarize_without_text(dataframe)
                return pd.merge(left=dataframe, right=dataframe_for_word2vec,
                                how="inner", left_on="index", right_on="index")

        return self.binarize_without_text(dataframe)
