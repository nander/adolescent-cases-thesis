import pandas as pd
from pathlib import Path
import os

PARENT_PATH = os.fspath(Path(__file__).parents[2])
API_FILE_PATH = os.path.join(PARENT_PATH,
                             "data_engineering",
                             "data",
                             "output.csv")
COMBINED_DATA_FILE_PATH = os.path.join(PARENT_PATH,
                                       "data",
                                       "combined_data_20180927.csv")


class TextDocumentsDB:
    def get_api_extracted_pdfs(self):
        """Extracts the API sentences that are in the files
        WODC provided.
        TODO: There must be and option also to retrieve the text
        in the PDF files WODC provided - for the Zaaknummers that
        are not found in the API call.
        """
        api_files = pd.read_csv(API_FILE_PATH, delimiter=",")
        if "Unnamed: 0" in api_files.columns:
            columns_to_be_selected = [
                col for col in api_files.columns if "Unnamed" not in col]
            return api_files.loc[:, columns_to_be_selected]
        else:
            return api_files


class CombinedDocumentDB:
    def retrieve_combined_documents(self):
        api_files = pd.read_csv(COMBINED_DATA_FILE_PATH, delimiter=",")
        if "Unnamed: 0" in api_files.columns:
            columns_to_be_selected = [
                col for col in api_files.columns if "Unnamed" not in col]
            return api_files.loc[:, columns_to_be_selected]
        else:
            return api_files
