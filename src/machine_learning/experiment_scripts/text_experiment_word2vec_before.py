from pathlib import Path
from sklearn.svm import SVC  # One versus one
from sklearn.linear_model import LogisticRegression
from lightgbm import LGBMClassifier
from sklearn.model_selection import GridSearchCV, StratifiedKFold
from sklearn.metrics import (
    f1_score, roc_auc_score, matthews_corrcoef,
    precision_score, recall_score, accuracy_score)
from sklearn.externals import joblib
import os
import numpy as np
import json
from datetime import datetime


# Imports from local.
from pipeline.model_pipeline import MakePipeline
from data_preprocessing import (
    prepare_dataset)
from utils.logger import set_logger


import warnings
warnings.filterwarnings("ignore")


CLF = "logistic_regression"
TYPE = "text_word2vec"
DUMPING_FILE_NAME = "A_{today}_{type_experiment}_{classifier_type}".format(
    today=datetime.now().strftime("%Y%m%d_%H%M%S"),
    classifier_type=CLF,
    type_experiment=TYPE)

PARENT_PATH = os.fspath(Path(__file__).parents[0])
PATH_TO_JSON = os.path.join(PARENT_PATH,
                            "experiment_results",
                            DUMPING_FILE_NAME+".json")

LOGGER = set_logger(DUMPING_FILE_NAME)
SEED = np.random.seed(42)

logistic_regression = LogisticRegression(n_jobs=-1)
svm_clf = SVC(probability=True, kernel="linear")
boosting = LGBMClassifier(objective="binary", n_jobs=-1, random_state=SEED)
boosting_parameters = {
    "classification__n_estimators": [75, 150, 300, 450],
    "classification__learning_rate": [0.001, 0.01, 0.1]}
svm_parameters = {
    "classification__C": 10. ** np.arange(-3, 3)}
logistic_regression_parameters = {
    "classification__penalty": ["l1", "l2"],
    "classification__C": [1, 10, 50, 100, 200, 300]}

# This needs to be changed in every iteration
CLASSIFIER_SET = logistic_regression
PARAMETER_SET = logistic_regression_parameters

# Text Experiment Settings: Text Only No Dimension Reduction
##########################
# Section combinations must be set to
# Content Details, Indictment, Evidence, Punishment,
# Decision and Full Text Combinations
# Full Text does not need to be combined with all
##########################
# Inside of the Pipeline
##########################
# Only selected sections are inserted into the Feature Union
# Keep text is true and sections are sections in the prepare dataset
# Skip dimension redcution is set to be True
# The Column Extractor for the column types are set to be None
# The pipeline will only construct a loop to experiment on the text sections

KEEP_TEXT = True
SELECTIONS = None
SKIP_DIMENSION_REDUCTION = True

section_combinations = [
    ["WIE"], ["WI"], ["WE"]]

dict_of_measurements = {}

for sect in section_combinations:

    SELECTIONS = sect  # Acts as if it is a db feature.

    # Indeed word2vec features are stored as db features.

    prep_data = prepare_dataset.DataPrep(
        tag=["Personal"],
        keep_text=KEEP_TEXT,
        sections=sect)  # This is only for labels

    sect = None  # Hacky way not to select the text features

    X = prep_data.data
    y = prep_data.labels

    # Balancing the dataset
    balanced_indices = np.append(
        np.argwhere(y == 1).ravel(),
        np.random.choice(
            np.argwhere(y == 0).ravel(),
            size=len(np.argwhere(y == 1).ravel())))

    X = X.loc[balanced_indices, :].reset_index(drop=True)
    y = y[balanced_indices]

    model = MakePipeline()

    pipe = model.set_sentence_classifier_pipeline(
        text_option=sect,  # Only selected sections from the judgements
        skip_dimension_reduction=SKIP_DIMENSION_REDUCTION,
        reduction_type="lda",
        n_components=100,
        selections=SELECTIONS,  # No column extraction
        classifier=CLASSIFIER_SET)

    grid_search = GridSearchCV(
        estimator=pipe, param_grid=PARAMETER_SET,
        n_jobs=-1, verbose=1, cv=5)

    kf = StratifiedKFold(n_splits=10, shuffle=True)

    auc_scores_list = []
    f1_scores_list = []
    mcc_scores_list = []
    precision_scores_list = []
    recall_scores_list = []
    accuracy_scores_list = []

    LOGGER.info("Experiment started for {a}...".format(a=SELECTIONS))
    LOGGER.info("Classifier is {b}.".format(
        b=CLF))
    LOGGER.info(
        "Total set consists of {c} instances...".format(c=X.shape[0]))
    # Stratified K fold is splitted here
    for train_index, test_index in kf.split(X, y):

        X_train, X_test = X.ix[train_index], X.ix[test_index]
        y_train, y_test = y[train_index], y[test_index]

        grid_search.fit(X_train, y_train)
        y_predicted = [x[1] for x in grid_search.predict_proba(X_test)]
        y_predicted_binary = grid_search.predict(X_test)
        temp_auc_score = roc_auc_score(y_true=y_test, y_score=y_predicted)
        temp_f1_score = f1_score(y_true=y_test, y_pred=y_predicted_binary)
        temp_mcc = matthews_corrcoef(
            y_true=y_test, y_pred=y_predicted_binary)
        temp_prec = precision_score(
            y_true=y_test, y_pred=y_predicted_binary)
        temp_recall = recall_score(
            y_true=y_test, y_pred=y_predicted_binary)
        temp_acc = accuracy_score(
            y_true=y_test, y_pred=y_predicted_binary)
        auc_scores_list.append(temp_auc_score)
        f1_scores_list.append(temp_f1_score)
        mcc_scores_list.append(temp_mcc)
        precision_scores_list.append(temp_prec)
        recall_scores_list.append(temp_recall)
        accuracy_scores_list.append(temp_acc)
        LOGGER.info("Best parameters: {}".format(grid_search.best_params_))
        LOGGER.info("Predicted:\n{}".format(y_predicted_binary))
        LOGGER.info("Real:\n{}".format(y_test))
        LOGGER.info("Indices:\n{}".format(test_index))
        LOGGER.info("F1 score:\n{}".format(temp_f1_score))

    MDL_DUMPING_NAME = "A_{type_experiment}_{today}_{classifier}_{s}".\
        format(
            today=datetime.now().strftime("%Y%m%d_%H%M%S"),
            classifier=CLF,
            type_experiment=TYPE,
            s=SELECTIONS)

    MDL_DUMP_PATH = os.path.join(PARENT_PATH,
                                 "experiment_results",
                                 "models_stored",
                                 MDL_DUMPING_NAME+".joblib")

    LOGGER.info(
        "Experiment finished for {a}...".format(a=SELECTIONS))
    LOGGER.info("Training for exporting the best model...")
    grid_search.fit(X, y)
    joblib.dump(grid_search, MDL_DUMP_PATH)
    LOGGER.info("Best model is exported, training is finished...")
    LOGGER.info("#" * 30)
    LOGGER.info("AUC mean: {}".format(np.array(auc_scores_list).mean()))
    LOGGER.info("AUC std: {}".format(np.array(auc_scores_list).std()))
    LOGGER.info("F1 mean: {}".format(np.array(f1_scores_list).mean()))
    LOGGER.info("F1 std: {}".format(np.array(f1_scores_list).std()))
    LOGGER.info("MCC mean: {}".format(np.array(mcc_scores_list).mean()))
    LOGGER.info("MCC std: {}".format(np.array(mcc_scores_list).std()))
    LOGGER.info("Accuracy mean: {}".format(
        np.array(accuracy_scores_list).mean()))
    LOGGER.info("Accuracy std: {}".format(
        np.array(accuracy_scores_list).std()))
    LOGGER.info("Precision mean: {}".format(
        np.array(precision_scores_list).mean()))
    LOGGER.info("Precision std: {}".format(
        np.array(precision_scores_list).std()))
    LOGGER.info("Recall mean: {}".format(
        np.array(recall_scores_list).mean()))
    LOGGER.info("Recall std: {}".format(
        np.array(recall_scores_list).std()))

    temp_inner_dict_for_overall_measurements = {
        "AUC": auc_scores_list,
        "F1": f1_scores_list,
        "MCC": mcc_scores_list,
        "Accuracy": accuracy_scores_list,
        "Recall": recall_scores_list,
        "Precision": precision_scores_list}

    dict_of_measurements.update(
        {temp_dict_key: temp_inner_dict_for_overall_measurements})

    with open(PATH_TO_JSON, "w") as fp:
        json.dump(dict_of_measurements, fp)

    LOGGER.info("Dictionary is dumped...")
