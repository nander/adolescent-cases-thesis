
from pathlib import Path
import os
import pandas as pd
import numpy as np
from pipeline.model_pipeline import TextFeaturePipeline
from pipeline.feature_construction import transform_the_text
import seaborn as sns
from sklearn.metrics.pairwise import cosine_similarity
from scipy.sparse import vstack
from sklearn.decomposition import TruncatedSVD
import matplotlib.pyplot as plt

PARENT_PATH = os.fspath(Path(os.getcwd()).parents[0])

tfp = TextFeaturePipeline(skip_dimension_reduction=True,
                          reduction_type=None, n_components=None)

PATH_FOR_TEXT_WITH_SECTIONS = os.path.join(
    PARENT_PATH,
    "data",
    "model_experiment",
    "dataset_full_20190204.csv")


cases_with_sections = pd.read_csv(PATH_FOR_TEXT_WITH_SECTIONS, sep="|")
SEED = np.random.seed(42)

selected_sections = ["content_details", "indictment",
                     "evidence", "punishment", "decision"]

df_for_analysis = cases_with_sections.loc[:,
                                          selected_sections]

labels_binary = [
    "A" if x == 0 else "J" for x in cases_with_sections.loc[:, "LABEL"].values]

df_for_analysis.columns = ["Content Details", "Indictment",
                           "Evidence", "Punishment", "Decision"]

full_text_to_dict = transform_the_text(
    " ".join(
        [df_for_analysis[col].str.cat(sep=', ')
         for col in df_for_analysis.columns]))

transformer = tfp.set_tf_idf()


transformer.fit([full_text_to_dict])

# Calculate similarities between the sections
#################################################################

transformed_matrices_dict = {}
for col in df_for_analysis.columns:
    transformed_matrix_temp = transformer.transform(df_for_analysis[col])
    transformed_matrices_dict.update({col: transformed_matrix_temp})

similarity_matrix = np.zeros((5, 5), dtype=float)
for idx_1, (col_1, matrix_1) in enumerate(
        transformed_matrices_dict.items()):
    for idx_2, (col_2, matrix_2) in enumerate(
            transformed_matrices_dict.items()):
        similarities_temp = np.array([])
        for f in range(matrix_1.shape[0]):
            sim_t = cosine_similarity(matrix_1[f], matrix_2[f])
            similarities_temp = np.append(similarities_temp, sim_t)
        similarity_matrix[idx_1, idx_2] = similarities_temp.mean()

labels = ["Content Details", "Indictment",
          "Evidence", "Punishment", "Decision"]

# Plot the heatmap and store for cosine similarities.
############################################################
corr = pd.DataFrame(similarity_matrix,
                    columns=transformed_matrices_dict.keys(),
                    index=transformed_matrices_dict.keys())

plt.style.use('seaborn-whitegrid')
fig, ax = plt.subplots(figsize=(12, 10))
sns.set(font_scale=1.8)
sns.heatmap(corr,
            xticklabels=labels,
            yticklabels=labels,
            cmap='Blues', fmt=".2g", annot=True, ax=ax)

plt.yticks(rotation=360, fontsize=14)
plt.xticks(fontsize=14)
plt.title("Similarity Matrix for Judgment Sections",
          fontsize=20)
plt.savefig(os.path.join(
    PARENT_PATH,
    "machine_learning",
    "plots",
    "similarity_matrix.png"), dpi=450)

#################################################################

labels_for_text = [col
                   for col in transformed_matrices_dict.keys()
                   for x in range(
                       transformed_matrices_dict["Indictment"].shape[0])]

labels_for_binary = [label
                     for col in transformed_matrices_dict.keys()
                     for label in labels_binary]
# Labels binary is A or J
# They repeat five times - number of the cols.

stacked_sparse_matrix = vstack(
    (transformed_matrices_dict["Content Details"],
     transformed_matrices_dict["Indictment"],
     transformed_matrices_dict["Evidence"],
     transformed_matrices_dict["Punishment"],
     transformed_matrices_dict["Decision"]))


# Plot the Truncated SVD
##################################################################
svd = TruncatedSVD(n_components=2, n_iter=20)

features_transformed = svd.fit_transform(stacked_sparse_matrix)

x = [element[0] for element in features_transformed]
y = [element[1] for element in features_transformed]

c_map = {'Evidence': 'green',
         'Indictment': 'orange',
         'Content Details': 'lightblue',
         'Punishment': 'yellow',
         'Decision': 'pink'}

plt.style.use('seaborn-whitegrid')
plt.figure(figsize=(12, 10))

plt.scatter(x, y, c=[c_map[_]
                     for _ in labels_for_text], alpha=0.8,
            edgecolors='black', s=32)

markers = [plt.Line2D([0, 0], [0, 0], color=color, marker='o',
                      linestyle='', markeredgecolor='black')
           for color in c_map.values()]

plt.legend(markers, c_map.keys(),
           bbox_to_anchor=(1.04, 0.5), loc="center left", borderaxespad=0)
plt.subplots_adjust(right=0.80)

plt.xlabel("First Component")
plt.ylabel("Second Component")
plt.title("SVD Features With Sections")
plt.savefig(os.path.join(
    PARENT_PATH,
    "machine_learning",
    "plots",
    "svd_features_text.png"), dpi=350, bbox_inches="tight")

plt.show()
##################################################################

# Plot the Truncated SVD with Colors of Labels
##################################################################
x = [element[0] for element in features_transformed]
y = [element[1] for element in features_transformed]

c_map = {'A': 'white',
         'J': 'red'}

plt.style.use('seaborn-whitegrid')
plt.figure(figsize=(12, 10))

plt.scatter(x, y, c=[c_map[_]
                     for _ in labels_for_binary], alpha=1,
            edgecolors='black', s=32)

markers = [plt.Line2D([0, 0], [0, 0], color=color, marker='o',
                      linestyle='', markeredgecolor='black')
           for color in c_map.values()]

plt.legend(markers, c_map.keys(),
           bbox_to_anchor=(1.04, 0.5), loc="center left", borderaxespad=0)
plt.subplots_adjust(right=0.80)

plt.xlabel("First Component")
plt.ylabel("Second Component")
plt.title("SVD Features With Case Outcomes")
plt.savefig(os.path.join(
    PARENT_PATH,
    "machine_learning",
    "plots",
    "svd_features_text_labels.png"), dpi=350)

plt.show()
##################################################################
