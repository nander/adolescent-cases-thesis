import logging
import os
from pathlib import Path


PARENT_PATH = os.fspath(Path(__file__).parents[2])
LOGGING_FILE_PATH = os.path.join(PARENT_PATH,
                                 "__logging",
                                 "{}.log")


def set_logger(file_path_extension):
    '''A logging helper.

    Keeps the logged experiments in the __logger path.

    Both prints out on the Terminal and writes on the
    .log file.'''
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)-7s: %(levelname)-1s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        handlers=[
            logging.FileHandler(LOGGING_FILE_PATH.format(file_path_extension)),
            logging.StreamHandler()
        ])
    return logging
