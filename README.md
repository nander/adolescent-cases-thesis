# Adolescent Criminal Law - Machine Learning Thesis

This is a machine learning and law thesis project written in Python.

The dataset is not shared with the Gitlab environment, while the pipelines are stored in versions.

The folder structure is as follows:
```
|-src
   |---data
   |-----model_experiment
   |---data_engineering
   |---imported_logs
   |---machine_learning
   |-----data_preprocessing
   |-----experiment_scripts
   |-----pipeline
   |-------pipeline_data
   |-----plots
   |-----stopwords
   |-----utils
```

There are two main modules in this project:

* `machine_learning`: includes the modules written for running the experiments for answering the thesis questions (Practical and Research). In the `machine_learning` module one can find 5 sub-modules and 1 folder that includes `plots`.
  * `data_preprocessing`: includes the source code for pre-processing the data kept in [/src/data/model_experiment](../src/data/model_experiment). As our structured dataset includes personal information, ethically we are not going to share our dataset.
  * `experiment_scripts`: includes the source code for all the different experiments done in the scope of this study. To run each script you need to extract those into [/src/machine_learning](../src/machine_learning) directory.
  * `pipeline`: includes the source code for the machine learning pipeline used in this thesis. There are 5 different dimension reduction settings:
    * LSA (See `TextFeaturePipeline` class and `reduction_type` method.)
    * LDA (See `TextFeaturePipeline` class and `reduction_type` method.)
    * Word2Vec (See `DBFeaturePipeline` class. We pre-calculated the weighted sum in Word2Vec dimension reduction method and stored into our updated model experiment dataset.)
    * LDA with Text Combinations (See `text_combination` parameter.)
  * `stop_words`: includes the pre-defined stopwords for Dutch.
  * `utils`: includes the `logging` source code for tracking many experiments in a Virtual Machine environment.
* `data_engineering`: includes the small application written by using `Selenium`. Using those sub-modules we scraped the Rechtspraak API. For more details in the application, please consult thesis.
